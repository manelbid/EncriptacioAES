package principal;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JFileChooser;

public class DecryptAES {

	private static final int IV_LENGTH = 16;

	/*
	 * A helper - to reuse the stream code below - if a small String is to be
	 * encrypted
	 */

	public static byte[] decrypt(String cipherText, String password)
			throws Exception {
		byte[] cipherTextBytes = cipherText.getBytes();
		ByteArrayInputStream bis = new ByteArrayInputStream(cipherTextBytes);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		decrypt(bis, bos, password);
		return bos.toByteArray();
	}

	public static void decrypt(InputStream in, OutputStream out, String password)
			throws Exception {

		byte[] iv = new byte[IV_LENGTH];
		in.read(iv);
		// System.out.println(">>>>>>>>red"+Arrays.toString(iv));

		Cipher cipher = Cipher.getInstance("AES/CFB8/NoPadding"); // "DES/ECB/PKCS5Padding";"AES/CBC/PKCS5Padding"
		SecretKeySpec keySpec = new SecretKeySpec(password.getBytes(), "AES");
		IvParameterSpec ivSpec = new IvParameterSpec(iv);
		cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);

		in = new CipherInputStream(in, cipher);
		byte[] buf = new byte[1024];
		int numRead = 0;
		while ((numRead = in.read(buf)) >= 0) {
			out.write(buf, 0, numRead);
		}
		out.close();
	}

	public static void copy(int mode, String inputFile, String outputFile,
			String password) throws Exception {

		BufferedInputStream is = new BufferedInputStream(new FileInputStream(
				inputFile));
		BufferedOutputStream os = new BufferedOutputStream(
				new FileOutputStream(outputFile));
		if (mode == Cipher.DECRYPT_MODE) {
			decrypt(is, os, password);
		} else
			throw new Exception("unknown mode");
		is.close();
		os.close();
	}

	public DecryptAES() {

		// Scanner scan = new Scanner(System.in);
		// String path= "";
		// System.out.println("Introdueix la ruta del fitxer");
		// path = scan.next();

		try {
			// check files - just for safety
			String fileName = FileChooser.arxiu2;

			String resultFileName = fileName + ".dec";

			File file = new File(fileName);
			if (!file.exists()) {
				System.out.println("No file " + fileName);
				return;
			}

			File file3 = new File(resultFileName);
			if (file3.exists()) {
				System.out
						.println("File for encrypted temp file or for the result decrypted file already exists. Please remove it or use a different file name");
				return;
			}

			copy(Cipher.DECRYPT_MODE, fileName, resultFileName,
					"&%e#7scSec@etKey");

			System.out
					.println("Success. Find decrypted file in current directory");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}