package principal;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptAES {

	private static final int IV_LENGTH = 16;

	/*
	 * A helper - to reuse the stream code below - if a small String is to be
	 * encrypted
	 */
	public static byte[] encrypt(String plainText, String password)
			throws Exception {
		ByteArrayInputStream bis = new ByteArrayInputStream(
				plainText.getBytes("UTF8"));
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		encrypt(bis, bos, password);
		return bos.toByteArray();
	}

	public static void encrypt(InputStream in, OutputStream out, String password)
			throws Exception {

		SecureRandom r = new SecureRandom();
		byte[] iv = new byte[IV_LENGTH];
		r.nextBytes(iv);
		out.write(iv); // write IV as a prefix
		out.flush();
		// System.out.println(">>>>>>>>written"+Arrays.toString(iv));

		Cipher cipher = Cipher.getInstance("AES/CFB8/NoPadding"); // "DES/ECB/PKCS5Padding";"AES/CBC/PKCS5Padding"
		SecretKeySpec keySpec = new SecretKeySpec(password.getBytes(), "AES");
		IvParameterSpec ivSpec = new IvParameterSpec(iv);
		cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

		out = new CipherOutputStream(out, cipher);
		byte[] buf = new byte[1024];
		int numRead = 0;
		while ((numRead = in.read(buf)) >= 0) {
			out.write(buf, 0, numRead);
		}
		out.close();
	}

	public static void copy(int mode, String inputFile, String outputFile,
			String password) throws Exception {

		BufferedInputStream is = new BufferedInputStream(new FileInputStream(
				inputFile));
		BufferedOutputStream os = new BufferedOutputStream(
				new FileOutputStream(outputFile));
		if (mode == Cipher.ENCRYPT_MODE) {
			encrypt(is, os, password);
		} else
			throw new Exception("unknown mode");
		is.close();
		os.close();
	}

	public EncryptAES() {

		try {
			// check files - just for safety
			String fileName = FileChooser.arxiu;
			String tempFileName = fileName + ".enc";

			File file = new File(fileName);
			if (!file.exists()) {
				System.out.println("No file " + fileName);
				return;
			}
			File file2 = new File(tempFileName);
			if (file2.exists()) {
				System.out
						.println("File for encrypted temp file or for the result decrypted file already exists. Please remove it or use a different file name");
				return;
			}

			copy(Cipher.ENCRYPT_MODE, fileName, tempFileName,
					"&%e#7scSec@etKey");

			System.out
					.println("Success. Find encrypted file in current directory");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
